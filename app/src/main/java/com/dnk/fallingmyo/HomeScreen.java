package com.dnk.fallingmyo;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;


public class HomeScreen extends ActionBarActivity {

    private String patientResponse = "0";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_screen);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home_screen, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void clickedHelp() {
        patientResponse = "HELP";
        //TODO: upload value of # to firebase account

    }

    public void clickedSpeedDial(){
        //TODO: call number in phone
    }

    public void clickedMeds(){
        patientResponse = "1";
        //TODO: upload value of response to firebase account
    }

    public void clickedFood() {
        patientResponse = "2";
        //TODO: upload value of response to firebase account
    }

    public void clickedWashroom(){
        patientResponse = "3";
        //TODO: upload value of response to firebase account
    }

    public void clickedLinens() {
        patientResponse = "4";
        //TODO: upload value of reponse to firebase account
    }

}
