package com.dnk.fallingmyo;

/**
 * Created by JeffreyHao-Chan on 2015-03-14.
 */
public class UnauthorizedUser {

    private String name;
    private String email;
    private String accountID;

    public UnauthorizedUser(String name, String email, String accountID) {
        this.name = name;
        this.email = email;
        this.accountID = accountID;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAccountID() {
        return this.accountID;
    }

    public void setAccountID(String accountID) {
        this.accountID = accountID;
    }


}
